/**
 * Returns the next bin day given the bin day of the week
 * @param {int} binDay day of the week (0 is sunday)
 * @param {Date} date (default is `new Date()`) date from which to find the next bin day
 * @returns {Date}
 */
export const getNextBinDay = (binDay, date = new Date()) => {
	const dayOfWeek = date.getDay();
    const until = Math.abs((dayOfWeek - (7 + binDay)) % 7);
    let nextBinDay = new Date();
    nextBinDay.setDate(nextBinDay.getDate() + until);
    return nextBinDay;
};
/**
 * Returns the number of days until the next bin day given
 * the bin day. Will always be less than 7.
 * @param {int} binDay day of the week (0 is sunday)
 * @param {Date} date (default is `new Date()`) date from which to find days until next bin day
 * @returns {int} 
 */
export const getDaysUntilNextBinDay = (binDay, date = new Date()) => {
    return Math.abs((date.getDay() - (7 + binDay)) % 7);
};
/**
 * Returns whether or not the given date is a recycle date
 * @param {Date} recycleDate a known date that is a recycle day in the bin cycle
 * @param {Date} date (default is `new Date()`) date to determine recycle date status
 */
export const isRecycleDay = (recycleDate, date = new Date()) =>{
	const days = Math.floor(((date - recycleDate) / (86400 * 1000)) % 14);
	return days === 0
}