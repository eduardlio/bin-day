import React, { useState } from "react";
import "./App.css";
import { getDaysUntilNextBinDay, getNextBinDay, isRecycleDay } from "./utils";

// Sunday is 0
const DEFAULT_CONFIG = {
    recycleDate: new Date(2020, 2, 31),
    binDay: 2
};
const DAY_SELECT_ACTIVE = false;

function App() {
    const [config, updateConfig] = useState(DEFAULT_CONFIG);
    const until = getDaysUntilNextBinDay(config.binDay);
    const nextBinDay = getNextBinDay(config.binDay);
    const nextBinDayIsRecycle = isRecycleDay(config.recycleDate, nextBinDay);
    const updateBinDay = day => {
        if (day < 7 && day >= 0) {
            updateConfig({ ...config, binDay: day });
        }
    };
    return (
        <div className="App">
            <div className="headerApp">
                <h1>
                    The next bin day is{" "}
                    {until > 1
                        ? `in ${until} days`
                        : until === 1
                        ? "tomorrow"
                        : "today"}{" "}
                    on {nextBinDay.toLocaleDateString("en-GB")}
                </h1>
                <h2>
                    It{!nextBinDayIsRecycle ? " won't" : "'ll"} be a recycle bin
                    day
                </h2>
            </div>
            {DAY_SELECT_ACTIVE && (
                <DaySelect
                    activeDay={config.binDay}
                    onDayChange={day => updateBinDay(day)}
                />
            )}
        </div>
    );
}
function DaySelect(props) {
    const days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ];
    return (
        <div className="daySelectWrap">
            <h3>Bin day is on :</h3>
            <div className="daySelect">
                {days.map((day, index) => {
                    return (
                        <p
                            className={
                                index === props.activeDay ? "activeDay" : null
                            }
                            key={index}
                            onClick={() => props.onDayChange(index)}
                        >
                            {day}
                        </p>
                    );
                })}
            </div>
        </div>
    );
}

export default App;
